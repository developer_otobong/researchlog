from django.apps import AppConfig


class ResearchLogsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'research_logs'
